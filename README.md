# iOS 10 Settings Launcher Widget #

* A simple Swift 3 iOS Widget project.
* Adjust color itself on power saving mode.
* Visual effects with blur and vibrancy.
* Universal app: support multiple devices.


## Preview: ##
Today Extensions:

![Widgets.jpg](https://bitbucket.org/repo/B9B5Kj/images/1312324376-Widgets.jpg)



App Screenshot:

![App.jpg](https://bitbucket.org/repo/B9B5Kj/images/4130135816-App.jpg)

## License: ##
* GPLv3
* You are NOT allowed to upload it to App Store.

## Copyright: ##
Copyright Â© 2007 - 2017 Hercy Chang. All rights reserved.
