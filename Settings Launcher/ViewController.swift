//
//  ViewController.swift
//  Settings Launcher
//
//  Created by Hercy Chang on 2/5/17.
//  Copyright © 2017 HAZ Inc. All rights reserved.
//
import Foundation
import UIKit
import CoreTelephony

extension UIView {
    
    
    func fadeIn(duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

    
class ViewController: UIViewController
{

    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func delay(_ delay:Double, closure:@escaping ()->())
    {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    
    weak var timer: Timer?  // Refresh timer
    weak var eyeBlinkTimer: Timer?  // Refresh timer
    //var HealthText: String = ""
    //var CellularText: String = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        /* Turn off info layer */
        InfoLayerView.isHidden = true;
        InfoLayerCloseBT.isEnabled = false
        InfoLayerView.alpha = 0.0
        
        updateAllBTs()
        
        
        /* Set refresh timer */
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(ViewController.updateAllBTs), userInfo: nil, repeats: true)
        
        /* Blink eyes every 5s */
        eyeBlinkTimer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(ViewController.blinkEyeAnimation), userInfo: nil, repeats: true)
        
        
        /* Initially set info labels on */
        labelOn = true;
        
        
        
        /* Fake double tap animation */
        delay(0.8)
        {
            // Down
            self.InfoBT.setImage(UIImage (named: "HBG Dark"), for: .normal);
            
            self.delay(0.15)
            {
                // Up
                self.InfoBT.setImage(UIImage (named: "BG Dark Bot"), for: .normal)
                
                self.delay(0.15)
                {
                    // Down
                    self.InfoBT.setImage(UIImage (named: "HBG Dark"), for: .normal)
                }
            }
        }
        
        
        /* Fade out labels */
        delay(3.0)
        {
            self.fadeOutAllLabels()
            
            /* Normalize label settings */
            self.delay(0.3)
            {
                self.setLablesAlphaTo(alpha: 1.0)
                self.displayLabels(show: false)
                self.labelOn = false
                self.InfoBT.setImage(UIImage (named: "BG Dark Bot"), for: .normal)
                
            }
        }

    }
    
    
    
    
    /* Blink eye on the info layer */
    @IBOutlet weak var eyeImage: UIImageView!
    func blinkEyeAnimation()
    {
        self.delay(0.1)
        {
            self.eyeImage.image = UIImage (named: "Eye Shut")
            self.delay(0.1)
            {
                self.eyeImage.image = UIImage (named: "Eye Open")
                self.delay(0.1)
                {
                    self.eyeImage.image = UIImage (named: "Eye Shut")
                    self.delay(0.1)
                    {
                        self.eyeImage.image = UIImage (named: "Eye Open")
                    }
                }
            }
        }
    }
    
    
    /* Fade out all labels */
    func fadeOutAllLabels()
    {
        self.WifiLabel.fadeOut()
        self.BluetoothLabel.fadeOut()
        self.LocationLabel.fadeOut()
        
        self.CellularLabel.fadeOut()
        self.BatteryLabel.fadeOut()
        self.AppRefreshLabel.fadeOut()
        
        self.StorageLabel.fadeOut()
        self.HealthLabel.fadeOut()
        self.NotificationLabel.fadeOut()
        
        self.AccessLabel.fadeOut()
        self.KeyboardLabel.fadeOut()
        self.PrivacyLabel.fadeOut()
        
        self.MessagesLabel.fadeOut()
        self.MailLabel.fadeOut()
        self.SafariLabel.fadeOut()
    }


    
    /* Set alpha value for all labels */
    func setLablesAlphaTo(alpha: CGFloat)
    {
        self.WifiLabel.alpha = alpha
        self.BluetoothLabel.alpha = alpha
        self.LocationLabel.alpha = alpha
        
        self.CellularLabel.alpha = alpha
        self.BatteryLabel.alpha = alpha
        self.AppRefreshLabel.alpha = alpha
        
        self.StorageLabel.alpha = alpha
        self.HealthLabel.alpha = alpha
        self.NotificationLabel.alpha = alpha
        
        self.AccessLabel.alpha = alpha
        self.KeyboardLabel.alpha = alpha
        self.PrivacyLabel.alpha = alpha
        
        self.MessagesLabel.alpha = alpha
        self.MailLabel.alpha = alpha
        self.SafariLabel.alpha = alpha
    }
    
    
    /* Info labels */
    @IBOutlet weak var WifiLabel: UILabel!
    @IBOutlet weak var BluetoothLabel: UILabel!
    @IBOutlet weak var LocationLabel: UILabel!
    
    @IBOutlet weak var CellularLabel: UILabel!
    @IBOutlet weak var BatteryLabel: UILabel!
    @IBOutlet weak var AppRefreshLabel: UILabel!
    
    @IBOutlet weak var StorageLabel: UILabel!
    @IBOutlet weak var HealthLabel: UILabel!
    @IBOutlet weak var NotificationLabel: UILabel!
    
    @IBOutlet weak var AccessLabel: UILabel!
    @IBOutlet weak var KeyboardLabel: UILabel!
    @IBOutlet weak var PrivacyLabel: UILabel!
    
    @IBOutlet weak var MessagesLabel: UILabel!
    @IBOutlet weak var MailLabel: UILabel!
    @IBOutlet weak var SafariLabel: UILabel!
    
    
    /* Label display status switch */
    func displayLabels(show: Bool)
    {
        var labels = [WifiLabel,
                      BluetoothLabel,
                      LocationLabel,
                      CellularLabel,
                      BatteryLabel,
                      AppRefreshLabel,
                      StorageLabel,
                      HealthLabel,
                      NotificationLabel,
                      AccessLabel,
                      KeyboardLabel,
                      PrivacyLabel,
                      MessagesLabel,
                      MailLabel,
                      SafariLabel]
        
        for i in 0...(labels.count - 1)
        {
            labels[i]?.isHidden = !show
        }
    }
    
    /* InfoBT gestures events */
    @IBOutlet weak var InfoBT: UIButton!
    var labelOn = false;
    @IBAction func InfoTouchDown(_ sender: Any)
    {
        displayLabels(show: true);
    }
    
    
    var alertOn = true;
    var clicks = 0;
    var currentTime = Date().timeIntervalSinceReferenceDate
    @IBAction func InfoTouchUpInside(_ sender: Any)
    {
        /*
        if(!labelOn)
        {
            displayLabels(show: false);
        }*/
        //else
        
        //{
        let preTime = currentTime
        currentTime = Date().timeIntervalSinceReferenceDate
        let diff = currentTime - preTime
        if(diff <= 3.0) // Clicks counter time out interval
        {
            clicks += 1
        }
        else
        {
            clicks = 0
        }
        NSLog("Click - %d, Diff: %f", clicks, diff);
        
        if(clicks == 5 && alertOn)    // Clicks activate the alert
        {
            alertOn = false
            NSLog("@ User clicked %d times!", clicks)
            
            
            // TODO
            // Show up a menu or something!!!
            displayInfoLayer()
            
        }
        self.delay(0.1)
        {
            self.fadeOutAllLabels()
            self.delay(0.3)
            {
                self.setLablesAlphaTo(alpha: 1.0)
                self.labelOn = false;
                self.displayLabels(show: false);
                self.InfoBT.setImage(UIImage (named: "BG Dark Bot"), for: .normal)
            }
        }
    }
    @IBAction func InfoTouchUpOutside(_ sender: Any)
    {
        if(!labelOn)
        {
            displayLabels(show: false);
        }
        //InfoBT.setImage(UIImage (named: "BG Dark Bot"), for: .normal)
        //labelOn = false
    }
    @IBAction func InfoTouchDragOut(_ sender: Any)
    {
        if(!labelOn)
        {
            displayLabels(show: false);
        }
    }
    @IBAction func InfoTocuDragIn(_ sender: Any)
    {
        if(!labelOn)
        {
            displayLabels(show: true);
        }
    }
    @IBAction func InfoExit(_ sender: Any)
    {
        if(!labelOn)
        {
            displayLabels(show: false);
        }
    }
    @IBAction func InfoCancel(_ sender: Any)
    {
        if(!labelOn)
        {
            displayLabels(show: false);
        }
        /*
        if(labelOn)
        {
            displayLabels(show: false);
            InfoBT.setImage(UIImage (named: "BG Dark Bot"), for: .normal)
            labelOn = false
        }*/
    }
    
    @IBAction func InfoBTDoubleTap(_ sender: UITapGestureRecognizer)
    {
        NSLog("DOUBLE TAPPED!!!")
        
        if(!labelOn)
        {
            labelOn = true;
            displayLabels(show: true);
            InfoBT.setImage(UIImage (named: "HBG Dark"), for: .normal)
        }
        else
        {
            labelOn = false;
            displayLabels(show: false);
            InfoBT.setImage(UIImage (named: "BG Dark Bot"), for: .normal)
        }
    }
    
    
    @IBAction func InfoBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        displayInfoLayer()
    }
    
    /* Wi-Fi Settings */
    @IBAction func WiFiBT(_ sender: UIButton)
    {
        NSLog("* Wi-Fi Settings:")
        urlOpenner(aURL: "app-Prefs:root=WIFI");
    }
    
    
    /* Cellular Settings */
    @IBOutlet weak var CellularICON: UIImageView!
    @IBOutlet weak var CellularBT: UIButton!
    @IBAction func CellularBT(_ sender: UIButton)
    {
        NSLog("* Cellular Settings:")
        
        // No cellular ability
        if(!hasCellular() && !isiPad())
        {
            // DO NOTHING
        }
        // iPad
        else if(isiPad())
        {
            urlOpenner(aURL: "app-Prefs:root=General&path=MULTITASKING");
        }
        // Has cellular ability
        else
        {
            urlOpenner(aURL: "app-Prefs:root=MOBILE_DATA_SETTINGS_ID");
        }
    }
    
    
    /* Location Settings */
    @IBAction func LocationBT(_ sender: UIButton)
    {
        NSLog("* Location Settings:")
        urlOpenner(aURL: "app-Prefs:root=Privacy&path=LOCATION");
    }
    @IBAction func LocationBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Find my friends")
        urlOpenner(aURL: "findmyfriends:");

    }
    
    
    /* App Refresh Settings */
    @IBAction func AppRefreshBT(_ sender: UIButton)
    {
        NSLog("* App Refresh Settings:")
        urlOpenner(aURL: "app-Prefs:root=General&path=AUTO_CONTENT_DOWNLOAD");
    }
    @IBAction func AppRefreshBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Software Updates Settings")
        urlOpenner(aURL: "app-Prefs:root=General&path=SOFTWARE_UPDATE_LINK");
    }
    
    
    /* Notification Settings */
    @IBAction func NotificationBT(_ sender: UIButton)
    {
        NSLog("* Notification Settings:")
        urlOpenner(aURL: "app-Prefs:root=NOTIFICATIONS_ID");
    }
    @IBAction func NotificationBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Reminders app:")
        urlOpenner(aURL: "x-apple-reminder://");
    }
    
    
    /* Battery Settings */
    @IBOutlet weak var BatteryBT: UIButton!
    @IBAction func BatteryBT(_ sender: UIButton)
    {
        NSLog("* Battery Settings:")
        urlOpenner(aURL: "app-Prefs:root=BATTERY_USAGE");
    }
    
    
    /* Storage Settings */
    @IBAction func StorageBT(_ sender: UIButton)
    {
        NSLog("* Storage Settings:")
        urlOpenner(aURL: "app-Prefs:root=General&path=STORAGE_ICLOUD_USAGE");
    }
    @IBAction func StorageBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* iCloud Settings:")
        urlOpenner(aURL: "app-Prefs:root=CASTLE");
    }
    
    
    /* Bluetooth Settings */
    @IBAction func BluetoothBT(_ sender: UIButton)
    {
        NSLog("* Storage Settings:")
        urlOpenner(aURL: "app-Prefs:root=Bluetooth");
    }
    
    
    /* Privacy Settings */
    @IBAction func PrivacyBT(_ sender: UIButton)
    {
        NSLog("* Privacy Settings:")
        urlOpenner(aURL: "app-Prefs:root=Privacy");
    }
    @IBAction func PrivacyBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Privacy Settings: Passcode")
        urlOpenner(aURL: "app-Prefs:root=DO_NOT_DISTURB");
    }
    
    
    /* Keyboard Settings */
    @IBAction func KeyboardBT(_ sender: UIButton)
    {
        NSLog("* Keyboard Settings:")
        urlOpenner(aURL: "app-Prefs:root=General&path=Keyboard");
    }
    @IBAction func KeyboardBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Keyboard Settings: Keyboard")
        urlOpenner(aURL: "app-Prefs:root=General&path=Keyboard/KEYBOARDS");
    }
    
    
    
    /* Accessibility Settings */
    @IBAction func Accessibility(_ sender: UIButton)
    {
        NSLog("* Accessibility Settings:")
        urlOpenner(aURL: "app-Prefs:root=General&path=ACCESSIBILITY");
    }
    @IBAction func AccessibilityBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Display Settings:")
        urlOpenner(aURL: "app-Prefs:root=DISPLAY");
    }
    
    
    /* Health App */
    @IBOutlet weak var HealthICON: UIImageView!
    @IBAction func HealthBT(_ sender: UIButton)
    {
        if(!isiPad())
        {
            NSLog("* Health App:")
            urlOpenner(aURL: "x-apple-health://");
        }
        else
        {
            NSLog("* Game Center Settings:")
            urlOpenner(aURL: "app-Prefs:root=GAMECENTER");
        }

    }
    

    /* Message App */
    @IBAction func MessageBT(_ sender: Any)
    {
        NSLog("* Messages App:")
        urlOpenner(aURL: "sms:open?message-guid=0://");
    }
    @IBAction func MessageBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Messages App: New message")
        urlOpenner(aURL: "sms://");

    }
    
    
    /* Mail App */
    @IBAction func EmailBT(_ sender: Any)
    {
        NSLog("* Mail App:")
        urlOpenner(aURL: "message://");

    }
    @IBAction func MailBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Mail App: New email")
        urlOpenner(aURL: "mailto:");
    }
    
    
    /* Safari App */
    @IBAction func SafariBT(_ sender: UIButton)
    {
        NSLog("* Safari App:")
        urlOpenner(aURL: "x-web-search://");
    }
    @IBAction func SafariBTLongPress(_ sender: Any)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Settings App: Safari")
        urlOpenner(aURL: "app-Prefs:root=SAFARI");

    }

    
    /* Longpress on SettingsBT */
    @IBAction func SettingsBT(_ sender: UIButton)
    {
        //NSLog("* LONG PRESSED!!!")
        NSLog("* Settings App:")
        urlOpenner(aURL: "app-Prefs:");
    }
    @IBAction func SettingsBTLongPress(_ sender: UILongPressGestureRecognizer)
    {
        NSLog("* LONG PRESSED!!!")
        NSLog("* Settings App:")
        urlOpenner(aURL: "app-Prefs:root=General");
    }
    
    
    
    
    /* URL Openner */
    func urlOpenner(aURL : String)
    {
        
        NSLog("* - Openning: " + aURL);
        let url = URL(string: aURL)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    /* Update all buttons */
    func updateAllBTs()
    {
        updateBatteryBT()
        updateCellularBT()
        updateHealthBT()
    }
    
    
    /* Update battery BG */
    func updateBatteryBT()
    {
        /* Change battery background color on power saving mode */
        if(ProcessInfo.processInfo.isLowPowerModeEnabled)
        {
            BatteryBT.setImage(UIImage (named: "BG Yellow"), for: .normal)
            BatteryBT.setImage(UIImage (named: "HBG Yellow"), for: .highlighted)
        }
        else
        {
            BatteryBT.setImage(UIImage (named: "BG Green"), for: .normal)
            BatteryBT.setImage(UIImage (named: "HBG Green"), for: .highlighted)
        }
    }
    
    
    /* Update Cellular BG */
    func updateCellularBT()
    {
        // No cellular ability
        if(!hasCellular() && !isiPad())
        {
            CellularBT.isEnabled = false;
            CellularICON.image = CellularICON.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            CellularICON.tintColor = UIColor.gray
            CellularLabel.text = "No Cellular"
            CellularLabel.textColor = UIColor.gray
        }
        else if(isiPad())
        {
            CellularBT.isEnabled = true;
            CellularBT.setImage(UIImage (named: "BG Green"), for: .normal)
            CellularICON.image = UIImage (named: "Multitasking")
            CellularLabel.text = "Multitasking"
            CellularLabel.textColor = UIColor.white
        }
        // Has cellular ability
        else
        {
            CellularBT.isEnabled = true;
            CellularBT.setImage(UIImage (named: "BG Green"), for: .normal)
            CellularICON.image = UIImage (named: "Cellular")
            CellularLabel.text = "Cellular"
            CellularLabel.textColor = UIColor.white
        }
    }
    
    
    /* Update Health BG */
    func updateHealthBT()
    {
        // No cellular ability
        if(isiPad())
        {
            //HealthICON.image = UIImage (named: "Cellular")
            //
            HealthICON.image = UIImage (named: "Game Center")
            HealthLabel.text = "Game Center"
        }
            // Has cellular ability
        else
        {
            HealthICON.image = UIImage (named: "Heart")
            HealthLabel.text = "Health"
        }
    }

    
    
    /* Check for cellular ability */
    private final func hasCellular() -> Bool
    {
        guard UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone else
        {
            return false
        }
        
        let mobileNetworkCode = CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode
        let isInvalidNetworkCode = mobileNetworkCode == nil || (mobileNetworkCode?.characters.count)! <= 0
            || mobileNetworkCode == "65535"
        //When sim card is removed, the Code is 65535
        
        return !isInvalidNetworkCode
    }

    /* Check current device is iPad */
    private final func isiPad() -> Bool
    {
        return UIDevice.current.userInterfaceIdiom == .pad
    }

    
    
    
    
    @IBOutlet weak var InfoLayerCloseBT: UIButton!
    /* Display Info Layer */
    @IBOutlet weak var InfoLayerView: UIVisualEffectView!
    func displayInfoLayer()
    {
        //InfoBT.isEnabled = false
        InfoLayerView.isHidden = false
        
        InfoLayerView.fadeIn()
        delay(0.5)
        {
            self.InfoLayerCloseBT.isEnabled = true
        }
    }
    
    /* Info Layer Turn off button */
    @IBAction func closeInfoLayer(_ sender: UIButton)
    {
        
        //InfoBT.isEnabled = true
        self.InfoLayerCloseBT.isEnabled = false
        
        InfoLayerView.fadeOut()
        
        delay(0.5)
        {
            self.InfoLayerView.isHidden = true
            
        }
    }
}

