//
//  infoViewController.swift
//  Settings Launcher
//
//  Created by Hercy Chang on 2/6/17.
//  Copyright © 2017 HAZ Inc. All rights reserved.
//

import UIKit

class InfoPageViewController: UIViewController, UIWebViewDelegate
{
    @IBOutlet weak var webViewCheater: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        let webUrl = URL(string: "http://hercy.me/Launcher_Widget/Demo.mov")!
        let webRequest : URLRequest = URLRequest(url: webUrl)
        webViewCheater.loadRequest(webRequest as URLRequest)
    }

    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}
