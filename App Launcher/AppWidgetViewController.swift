//
//  TodayViewController.swift
//  Widget
//
//  Created by Hercy Chang on 2/8/17.
//  Copyright © 2017 HAZ Inc. All rights reserved.
//

import UIKit
import NotificationCenter

import CoreTelephony



class AppWidgetViewController: UIViewController, NCWidgetProviding
{
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    weak var timer: Timer?  // Refresh timer
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        /* Set refresh timer */
        //timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(TodayViewController.updateBGs), userInfo: nil, repeats: true)
        
        
        
    }
    
    
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void))
    {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        completionHandler(NCUpdateResult.newData)
        
    }
    
    // Safari Button
    @IBAction func SafariLBT(_ sender: UIButton)
    {
        NSLog("* Safari:")
        urlOpenner(aURL: "x-web-search:");
    }
    
    // Maps Button
    @IBAction func MapsBT(_ sender: UIButton)
    {
        NSLog("* Maps:")
        urlOpenner(aURL: "http://maps.apple.com/?q");
    }
    
    // iCloud Drive Button
    @IBAction func iCloudDriveBT(_ sender: UIButton)
    {
        NSLog("* iCloud Drive:")
        urlOpenner(aURL: "Iclouddriveapp://");
    }
    
    // Music Button
    @IBAction func MusicBT(_ sender: UIButton)
    {
        NSLog("* Music:")
        urlOpenner(aURL: "music:");
    }
    
    // Notes Button
    @IBAction func NotesBT(_ sender: UIButton)
    {
        NSLog("* Notes:")
        urlOpenner(aURL: "mobilenotes:");
    }
    
    
    /* URL Openner */
    func urlOpenner(aURL : String)
    {
        
        NSLog("* - Openning: " + aURL);
        let myAppUrl = NSURL(string: aURL)!
        extensionContext?.open(myAppUrl as URL, completionHandler: { (success) in
            if (!success)
            {
                // Unable to open this URL
                NSLog("* - Failed!");
            }
            else
            {
                NSLog("* - Done!");
            }
        })
        
    }
    
}
