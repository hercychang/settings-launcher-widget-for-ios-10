//
//  TodayViewController.swift
//  Widget
//
//  Created by Hercy Chang on 2/5/17.
//  Copyright © 2017 HAZ Inc. All rights reserved.
//

import UIKit
import NotificationCenter

import CoreTelephony



class TodayViewController: UIViewController, NCWidgetProviding
{
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    weak var timer: Timer?  // Refresh timer
    var BluetoothText: String = ""
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view from its nib.
        
        /* Set refresh timer */
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(TodayViewController.updateBGs), userInfo: nil, repeats: true)
        
        BluetoothText = BluetoothBT.title(for: .normal)!
        //updateBGs();
        
    }
    
    
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void))
    {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        
        updateBGs();
        completionHandler(NCUpdateResult.newData)
        updateBGs();
    }
    
    
    
    
    /* Wi-Fi Settings */
    @IBAction func WiFiBT(_ sender: UIButton)
    {
        NSLog("* Wi-Fi Settings:")
        urlOpenner(aURL: "app-Prefs:root=WIFI");
        
    }
    
    
    /* Cellular Settings */
    @IBOutlet weak var CellularIcon: UIImageView!
    @IBOutlet weak var CellularBG: UIImageView!
    @IBAction func CellularBT(_ sender: UIButton)
    {
        
        // Not iPhone
        if(!hasCellularAbility())
        {
            NSLog("* Bluetooth Settings:")
            urlOpenner(aURL: "app-Prefs:root=Bluetooth");
        }
            // iPhone
        else
        {
            NSLog("* Cellular Settings:")
            urlOpenner(aURL: "app-Prefs:root=MOBILE_DATA_SETTINGS_ID");
        }
    }
    
    
    /* Location Settings */
    @IBAction func LocationBT(_ sender: UIButton)
    {
        NSLog("* Location Settings:")
        urlOpenner(aURL: "app-Prefs:root=Privacy&path=LOCATION");
        
    }
    
    
    /* App Refresh Settings */
    @IBAction func AppRefreshBT(_ sender: UIButton)
    {
        NSLog("* App Refresh Settings:")
        urlOpenner(aURL: "app-Prefs:root=General&path=AUTO_CONTENT_DOWNLOAD");
        
    }
    
    
    /* Notification Settings */
    @IBAction func NotificationBT(_ sender: UIButton)
    {
        NSLog("* Notification Settings:")
        urlOpenner(aURL: "app-Prefs:root=NOTIFICATIONS_ID");
        
    }
    
    
    /* Battery Settings */
    @IBOutlet weak var BatteryBG: UIImageView!
    @IBAction func BatteryBT(_ sender: UIButton)
    {
        NSLog("* Battery Settings:")
        urlOpenner(aURL: "app-Prefs:root=BATTERY_USAGE");
        
    }
    
    
    /* Storage Settings */
    @IBAction func StorageBT(_ sender: UIButton)
    {
        NSLog("* Storage Settings:")
        urlOpenner(aURL: "app-Prefs:root=General&path=STORAGE_ICLOUD_USAGE");
        
    }
    
    
    /* Bluetooth Settings */
    @IBOutlet weak var BluetoothBT: UIButton!
    @IBAction func BluetoothBT(_ sender: UIButton)
    {
        // Not iPhone
        if(!hasCellularAbility())
        {
            NSLog("* Open Settings App:")
            urlOpenner(aURL: "app-Prefs:root=General");
        }
            // iPhone
        else
        {
            NSLog("* Bluetooth Settings:")
            urlOpenner(aURL: "app-Prefs:root=Bluetooth");
        }
        
    }
    
    
    /* URL Openner */
    func urlOpenner(aURL : String)
    {
        updateBGs();
        
        NSLog("* - Openning: " + aURL);
        let myAppUrl = NSURL(string: aURL)!
        extensionContext?.open(myAppUrl as URL, completionHandler: { (success) in
            if (!success)
            {
                // Unable to open this URL
                NSLog("* - Failed!");
            }
            else
            {
                NSLog("* - Done!");
            }
        })
        
        updateBGs();
    }
    
    
    /* Update all BGs */
    func updateBGs()
    {
        replaceCellularBT()
        replaceBluetoothBT()
        updateBatteryBG()
    }
    
    
    /* Update battery BG */
    func updateBatteryBG()
    {
        /* Change battery background color on power saving mode */
        if(ProcessInfo.processInfo.isLowPowerModeEnabled)
        {
            BatteryBG.image = UIImage(named: "Yellow")
        }
        else
        {
            BatteryBG.image = UIImage(named: "Green")
        }
    }
    
    
    func replaceCellularBT()
    {
        // Not iPhone
        if(!hasCellularAbility())
        {
            CellularIcon.image = UIImage(named: "Bluetooth")
            CellularBG.image = UIImage(named: "Blue")
        }
            // iPhone
        else
        {
            CellularIcon.image = UIImage(named: "Cellular")
            CellularBG.image = UIImage(named: "Green")
        }
    }
    
    func replaceBluetoothBT()
    {
        // Not iPhone
        if(!hasCellularAbility())
        {
            BluetoothBT.setTitle("General", for: .normal);
        }
            // iPhone
        else
        {
            BluetoothBT.setTitle(BluetoothText, for: .normal);
            //BluetoothBT.setTitle("Bluetooth", for: .normal);
        }
    }
    
    
    
    private final func hasCellularAbility() -> Bool
    {
        guard UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone else
        {
            return false
        }
        
        let mobileNetworkCode = CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode
        let isInvalidNetworkCode = mobileNetworkCode == nil || (mobileNetworkCode?.characters.count)! <= 0
            || mobileNetworkCode == "65535"
        //When sim card is removed, the Code is 65535
        
        return !isInvalidNetworkCode
    }
    
}
